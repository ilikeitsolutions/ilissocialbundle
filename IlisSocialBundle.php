<?php

namespace Ilis\Bundle\SocialBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Ilis Social Bundle
 */
class IlisSocialBundle extends Bundle
{
}
