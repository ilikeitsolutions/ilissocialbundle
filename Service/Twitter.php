<?php
/**
 * Created by JetBrains PhpStorm.
 * User: juancarlos.romero
 * Date: 3/08/13
 * Time: 0:04
 * To change this template use File | Settings | File Templates.
 */

namespace Ilis\Bundle\SocialBundle\Service;

use Guzzle\Http\Client;
use Guzzle\Plugin\Oauth\OauthPlugin;

/**
 * Ilis\Bundle\SocialBundle\Service\Twitter
 *
 */

class Twitter
{

    private $baseUrl;
    private $name;
    private $consumerKey;
    private $consumerSecret;
    private $token;
    private $tokenSecret;
    private $maxResults;


    /**
     * Set baseUrl
     *
     * @param string $baseUrl
     *
     * @return $this
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set consumerKey
     *
     * @param string $consumerKey
     *
     * @return $this
     */
    public function setConsumerKey($consumerKey)
    {
        $this->consumerKey = $consumerKey;

        return $this;
    }

    /**
     * Set consumerSecret
     *
     * @param string $consumerSecret
     *
     * @return $this
     */
    public function setConsumerSecret($consumerSecret)
    {
        $this->consumerSecret = $consumerSecret;

        return $this;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Set tokenSecret
     *
     * @param string $tokenSecret
     *
     * @return $this
     */
    public function setTokenSecret($tokenSecret)
    {
        $this->tokenSecret = $tokenSecret;

        return $this;
    }

    /**
     * Set maxResults
     *
     * @param integer $maxResults
     *
     * @return $this
     */
    public function setMaxResults($maxResults = 1)
    {
        $this->maxResults = $maxResults;

        return $this;
    }

    /**
     * Get twitter array data
     *
     * @return array
     */
    public function getData(){

        $client = new Client($this->baseUrl);
        $oauth = new OauthPlugin(array(
            'consumer_key'    => $this->consumerKey,
            'consumer_secret' => $this->consumerSecret,
            'token'           => $this->token,
            'token_secret'    => $this->tokenSecret,
        ));
        $client->addSubscriber($oauth);

        $search = $client->get('statuses/user_timeline.json');
        $search->setHeader('Accept', 'application/json');
        $search->getQuery()->set('screen_name',$this->name);
        $search->getQuery()->set('count',  $this->maxResults);

        $response = $search->send();
        $tweets_res = json_decode($response->getBody(true), true);

        foreach($tweets_res as $tweet){
            $current_tweet = array();
            $current_tweet['id'] = $tweet['id'];
            $current_tweet['created_at'] = $tweet['created_at'];
            if (isset($tweet['retweeted_status'])){
                $current_tweet['text'] = 'RT @'.$tweet['retweeted_status']['user']['screen_name'].': '.$tweet['retweeted_status']['text'];
            } else {
                $current_tweet['text'] = $tweet['text'];
            }
            $tweets[] = $current_tweet;
        }

        return $tweets;
    }


}